# Ukrainian translation
# Copyright (C) 2021 VideoLAN
# This file is distributed under the same license as the vlc package.
#
# Translators:
# Андрій Бандура <andriykopanytsia@gmail.com>, 2013-2017
msgid ""
msgstr ""
"Project-Id-Version: VideoLAN's websites\n"
"Report-Msgid-Bugs-To: vlc-devel@videolan.org\n"
"POT-Creation-Date: 2020-02-05 18:13+0100\n"
"PO-Revision-Date: 2017-12-09 05:40+0100\n"
"Last-Translator: Андрій Бандура <andriykopanytsia@gmail.com>, 2017\n"
"Language-Team: Ukrainian (http://www.transifex.com/yaron/vlc-trans/language/"
"uk/)\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != "
"11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % "
"100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || "
"(n % 100 >=11 && n % 100 <=14 )) ? 2: 3);\n"

#: include/header.php:289
msgid "a project and a"
msgstr "проект та"

#: include/header.php:289
msgid "non-profit organization"
msgstr "неприбуткова організація"

#: include/header.php:298 include/footer.php:79
msgid "Partners"
msgstr "Партнери"

#: include/menus.php:32
msgid "Team &amp; Organization"
msgstr "Команда і організація"

#: include/menus.php:33
msgid "Consulting Services &amp; Partners"
msgstr "Консультуючі служби та партнери"

#: include/menus.php:34 include/footer.php:82
msgid "Events"
msgstr "Події"

#: include/menus.php:35 include/footer.php:77 include/footer.php:111
msgid "Legal"
msgstr "Правова інформація"

#: include/menus.php:36 include/footer.php:81
msgid "Press center"
msgstr "Прес-релізи"

#: include/menus.php:37 include/footer.php:78
msgid "Contact us"
msgstr "Зв'яжіться з нами"

#: include/menus.php:43 include/os-specific.php:279
msgid "Download"
msgstr "Завантажити"

#: include/menus.php:44 include/footer.php:35
msgid "Features"
msgstr "Функції"

#: include/menus.php:45 vlc/index.php:57 vlc/index.php:63
msgid "Customize"
msgstr "Підлаштування"

#: include/menus.php:47 include/footer.php:69
msgid "Get Goodies"
msgstr "Отримати смаколики"

#: include/menus.php:51
msgid "Projects"
msgstr "Проекти"

#: include/menus.php:71 include/footer.php:41
msgid "All Projects"
msgstr "Усі проекти"

#: include/menus.php:75 index.php:168
msgid "Contribute"
msgstr "Співпраця"

#: include/menus.php:77
msgid "Getting started"
msgstr "Початок"

#: include/menus.php:78 include/menus.php:96
msgid "Donate"
msgstr "Пожертва"

#: include/menus.php:79
msgid "Report a bug"
msgstr "Повідомити про помилку"

#: include/menus.php:83
msgid "Support"
msgstr "Підтримка"

#: include/footer.php:33
msgid "Skins"
msgstr "Обкладинки"

#: include/footer.php:34
msgid "Extensions"
msgstr "Розширення"

#: include/footer.php:36 vlc/index.php:83
msgid "Screenshots"
msgstr "Знімки екрану"

#: include/footer.php:61
msgid "Community"
msgstr "Спільнота"

#: include/footer.php:64
msgid "Forums"
msgstr "Форуми"

#: include/footer.php:65
msgid "Mailing-Lists"
msgstr "Поштові розсилки"

#: include/footer.php:66
msgid "FAQ"
msgstr "ЧАП"

#: include/footer.php:67
msgid "Donate money"
msgstr "Пожертвуйте гроші"

#: include/footer.php:68
msgid "Donate time"
msgstr "Пожертвуйте час"

#: include/footer.php:75
msgid "Project and Organization"
msgstr "Проект та організація"

#: include/footer.php:76
msgid "Team"
msgstr "Команда"

#: include/footer.php:80
msgid "Mirrors"
msgstr "Дзеркала"

#: include/footer.php:83
msgid "Security center"
msgstr "Центр безпеки"

#: include/footer.php:84
msgid "Get Involved"
msgstr "Допомогти у розробці"

#: include/footer.php:85
msgid "News"
msgstr "Новини"

#: include/os-specific.php:103
msgid "Download VLC"
msgstr "Завантажити VLC"

#: include/os-specific.php:109 include/os-specific.php:295 vlc/index.php:168
msgid "Other Systems"
msgstr "Інші Системи"

#: include/os-specific.php:260
msgid "downloads so far"
msgstr "завантажень"

#: include/os-specific.php:648
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and "
"various streaming protocols."
msgstr ""
"VLC є безкоштовним з відкритим  вихідним кодом  багатоплатформним "
"мультимедійним програвачем та середовищем, яке відтворює більшість "
"мультимедійних файлів, а також DVD, аудіо CD, VCD та різноманітні потокові "
"протоколи."

#: include/os-specific.php:652
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files, and various streaming protocols."
msgstr ""
"VLC є безкоштовним з відкритим  вихідним кодом  багатоплатформним "
"мультимедійним програвачем та середовищем, яке відтворює більшість "
"мультимедійних файлів та різноманітні потокові протоколи."

#: index.php:4
msgid "VLC: Official site - Free multimedia solutions for all OS!"
msgstr "VLC: Офіційний сайт - безкоштовні мультимедійні рішення для всіх ОС!"

#: index.php:26
msgid "Other projects from VideoLAN"
msgstr "Інші проекти від VideoLAN"

#: index.php:30
msgid "For Everyone"
msgstr "Для кожного"

#: index.php:40
msgid ""
"VLC is a powerful media player playing most of the media codecs and video "
"formats out there."
msgstr ""
"VLC - це потужний медіапрогравач, який відтворює більшість існуючих медіа-"
"кодеків та форматів відео."

#: index.php:53
msgid ""
"VideoLAN Movie Creator is a non-linear editing software for video creation."
msgstr "VideoLAN Movie Creator - це нелінійний редактор для створення відео."

#: index.php:62
msgid "For Professionals"
msgstr "Для професіоналів"

#: index.php:72
msgid ""
"DVBlast is a simple and powerful MPEG-2/TS demux and streaming application."
msgstr ""
"DVBlast - це простий і потужний MPEG-2/TS демультиплексор і потокова "
"програма."

#: index.php:82
msgid ""
"multicat is a set of tools designed to easily and efficiently manipulate "
"multicast streams and TS."
msgstr ""
"multicat - це набір інструментів, призначених для легкого і ефективного "
"управління багатоадресними і транспортними потоками."

#: index.php:95
msgid ""
"x264 is a free application for encoding video streams into the H.264/MPEG-4 "
"AVC format."
msgstr ""
"x264 - це безкоштовна програма для кодування відеопотоків у форматі H.264/"
"MPEG-4 AVC."

#: index.php:104
msgid "For Developers"
msgstr "Для розробників"

#: index.php:140
msgid "View All Projects"
msgstr "Переглянути усі проекти"

#: index.php:144
msgid "Help us out!"
msgstr "Допоможіть нам!"

#: index.php:148
msgid "donate"
msgstr "пожертва"

#: index.php:156
msgid "VideoLAN is a non-profit organization."
msgstr "VideoLAN - це неприбуткова організація."

#: index.php:157
msgid ""
" All our costs are met by donations we receive from our users. If you enjoy "
"using a VideoLAN product, please donate to support us."
msgstr ""
"Всі наші витрати покриваються за рахунок пожертвувань, які ми отримуємо від "
"наших користувачів. Якщо вам подобається користуватися програмою від "
"VideoLAN, будь ласка, пожертвуйте, щоб підтримати нас."

#: index.php:160 index.php:180 index.php:198
msgid "Learn More"
msgstr "Дізнатися більше"

#: index.php:176
msgid "VideoLAN is open-source software."
msgstr "VideoLAN - це відкрите програмне забезпечення."

#: index.php:177
msgid ""
"This means that if you have the skill and the desire to improve one of our "
"products, your contributions are welcome"
msgstr ""
"Це означає, що якщо у вас є навички і бажання поліпшити один з наших "
"продуктів, то ваш внесок вітається"

#: index.php:187
msgid "Spread the Word"
msgstr "Розкажіть про нас"

#: index.php:195
msgid ""
"We feel that VideoLAN has the best video software available at the best "
"price: free. If you agree please help spread the word about our software."
msgstr ""
"Ми вважаємо, що VideoLAN має найкраще програмне забезпечення для відео, "
"доступне за найкращою ціною: безкоштовне. Якщо ви згідні з нами, то "
"допоможіть поширити інформацію про наше програмне забезпечення."

#: index.php:215
msgid "News &amp; Updates"
msgstr "Новини і оновлення"

#: index.php:218
msgid "More News"
msgstr "Більше новин"

#: index.php:222
msgid "Development Blogs"
msgstr "Блог розробників"

#: index.php:251
msgid "Social media"
msgstr "Соціальні мережі"

#: vlc/index.php:3
msgid "Official download of VLC media player, the best Open Source player"
msgstr ""
"Офіційне завантаження медіапрогравача VLC, найкращого з відкритим вихідним "
"кодом"

#: vlc/index.php:21
msgid "Get VLC for"
msgstr "Отримати VLC для"

#: vlc/index.php:29 vlc/index.php:32
msgid "Simple, fast and powerful"
msgstr "Простий, швидкий і потужний"

#: vlc/index.php:35
msgid "Plays everything"
msgstr "Відтворює усе"

#: vlc/index.php:35
msgid "Files, Discs, Webcams, Devices and Streams."
msgstr "Файли, диски, веб-камери, пристрої і мережні потоки."

#: vlc/index.php:38
msgid "Plays most codecs with no codec packs needed"
msgstr ""
"Відтворює більшість кодеків без необхідності встановлення додаткових "
"пакунків кодеків"

#: vlc/index.php:41
msgid "Runs on all platforms"
msgstr "Працює на всіх платформах"

#: vlc/index.php:44
msgid "Completely Free"
msgstr "Повністю вільний"

#: vlc/index.php:44
msgid "no spyware, no ads and no user tracking."
msgstr "без шпигунства, без реклами і немає стеження за користувачами."

#: vlc/index.php:47
msgid "learn more"
msgstr "дізнатися більше"

#: vlc/index.php:66
msgid "Add"
msgstr "Додати"

#: vlc/index.php:66
msgid "skins"
msgstr "обкладинки"

#: vlc/index.php:69
msgid "Create skins with"
msgstr "Створити обкладинку у програмі"

#: vlc/index.php:69
msgid "VLC skin editor"
msgstr "Редактор обкладинок VLC"

#: vlc/index.php:72
msgid "Install"
msgstr "Встановити"

#: vlc/index.php:72
msgid "extensions"
msgstr "розширення"

#: vlc/index.php:126
msgid "View all screenshots"
msgstr "Переглянути усі знімки екрану"

#: vlc/index.php:135
msgid "Official Downloads of VLC media player"
msgstr "Офіційні завантаження медіапрогравача VLC"

#: vlc/index.php:146
msgid "Sources"
msgstr "Вихідний код"

#: vlc/index.php:147
msgid "You can also directly get the"
msgstr "Ви також можете отримати"

#: vlc/index.php:148
msgid "source code"
msgstr "джерельний код"

#~ msgid "Can do media conversion and streaming."
#~ msgstr "Може  перетворювати відео і передавати потік."

#~ msgid "Discover all features"
#~ msgstr "Дізнатися про всі можливості"

#~ msgid "A project and a"
#~ msgstr "Проект та"

#~ msgid ""
#~ "composed of volunteers, developing and promoting free, open-source "
#~ "multimedia solutions."
#~ msgstr ""
#~ "створені добровольцями, які розробляють і розповсюджують вільні, з "
#~ "відкритим вихідним кодом мультимедійні рішення."

#~ msgid "why?"
#~ msgstr "чому?"

#~ msgid "Home"
#~ msgstr "Головна"

#~ msgid "Support center"
#~ msgstr "Центр підтримки"

#~ msgid "Dev' Zone"
#~ msgstr "Розробникам"

#~ msgid "DONATE"
#~ msgstr "ПОЖЕРТВА"

#~ msgid "Other Systems and Versions"
#~ msgstr "Інші системи та версії"

#~ msgid "Other OS"
#~ msgstr "Інші ОС"
